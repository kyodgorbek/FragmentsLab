package com.example.fragmentslab;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class MainActivity extends ActionBarActivity
        implements ContentRequestListener {
    public static final int PREVIEW_REQUEST_CODE = 253;

    private PreviewFragment mFragPreview;
    private UriListFragment mFragList

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        mFragPreview = (PreviewFragment) fragmentManager.findFragmentById(R.id.fragPreview);
        mFragList = (UrlListFragment) fragmentManager.findFragmentById(R.id.fragList);
    }

    @Override
    public  void contentChanged(String newContent) {
     // when in single pane node mFragPreview will be null
      if (mFragPreview != null) {
          mFragPreview.onContentChangeRequest(newContent);
      }

     @Override
     public void contentChanged(String newContent){
            //when in single pane mode mFragPreview will be null
            if (mFragPreview != null) {
                mFragPreview.onContentChangeRequest(newContent);
            } else {
                Intent previewIntent = new Intent(MainActivity.this, PreviewActivity.class);
                previewIntent.putExtra(PreviewActivity.URL_BUNDLE_KEY, newContent);
                MainActivity.this.startActivityForResult(previewIntent, PREVIEW_REQUEST_CODE);
            }
        }


     @Override
             public void addNewContentItem(String item){
             if(mFragList != null){
             mFragList.onItemAdded(item);
        }
    }

    @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PREVIEW_REQUEST_CODE && resultCode == RESULT_OK &&
              data != null && data.hasExtra(PreviewActivity.URL_BUNDLE_KEY) ){
           addNewContentItem(data.getStringExtra(PreviewActivity.URL_BUNDLE_KEY));
        }
        