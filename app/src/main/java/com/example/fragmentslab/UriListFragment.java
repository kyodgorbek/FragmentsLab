package com.example.fragmentslab;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class UriListFragment extends android.support.v4.app.ListFragment {

  private ContentRequestListener mContentListener;
  private ArrayList<String> mItemList;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mItemList = new ArrayList<string>
      (Arrays.asList(getResources().getStringArray(R.array.url_string_array)));
      ArrayAdapter<String> adapter = new ArrayAdapter<~>(getActivity(),
           android.R.layout.simple_list_item_1, android.R.id.text1,
           mItemList;
      setListAdapter(adapter);
  }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContentListener = (ContentRequestListenr) activity;
    }

    @Override
    public void onListItemClick(ListView 1, View v, int position, long id){
        mContentListener.contentChanged((String) 1.getAdapter().getItem(position));

  }
