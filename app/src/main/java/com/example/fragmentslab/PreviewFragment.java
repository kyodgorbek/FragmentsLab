package com.example.fragmentslab;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class PreviewFragment extends android.support.v4.app.Fragment {

    private WebView mPreview
    private EditText mNewUrl;
    private ContextRequestListener mRequestListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview container, false);
        mPreview = (WebView) view.findViewById(R.id.wvPreview);
        mPreview.getSettings().setJavaScriptEnabled(true);
        mPreview.setWebViewClient(new WebViewClient());

        mNewUrl = (EditText) view.findViewById(R.id.etNewUrl);
        ImageButton addNewUrl = (ImageButton) view.findViewById(R.id.btnAdd);
        addNewUrl.setOnClickListener((v) -> {
            mPreview.addNewContentItem(mNewUrl.getText().toString());

        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mRequestListener = (ContentRequestListener) activity;
    }

    public void onContentChangeRequest(String newUrl) {
        mPreview.loadUrl(newUrl);
    }
}